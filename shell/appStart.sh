#!/bin/bash

# 应用名称数组
APPLICATIONS=(
  'His-ERP-Service-1.0.jar'
  'His-Doctor-Service-1.0.jar'
  'His-Statistic-Service-1.0.jar'
  'His-System-Web-1.0.jar'
)
# 应用名称数组
APPLICATIONS_PATH=(
  './code/Open-His/His-ERP/His-ERP-Service/target/His-ERP-Service-1.0.jar'
  './code/Open-His/His-Doctor/His-Doctor-Service/target/His-Doctor-Service-1.0.jar'
  './code/Open-His/His-Statistic/His-Statistic-Service/target/His-Statistic-Service-1.0.jar'
  './code/Open-His/His-System/His-System-Web/target/His-System-Web-1.0.jar'
)

# ${#APPLICATIONS_PATH[@]} 表示数组长度
for ((i=0; i<${#APPLICATIONS_PATH[@]}; i++ )) do
  # 启动jar包
  `nohup java -jar ${APPLICATIONS_PATH[i]} >./${APPLICATIONS[i]}.log 2>&1 &`
  # 休息3秒
  sleep 3s
  # 检查进程是否存在
  pid=`ps -ef | grep ${APPLICATIONS[i]} | grep 'java -jar' | awk '{printf $2}'`
  if [ -z $pid ];
    then
      echo "应用：${APPLICATIONS[i]} 没有运行!"
    else
      echo "应用：${APPLICATIONS[i]} 正在运行... 进程:$pid "
  fi
done;
