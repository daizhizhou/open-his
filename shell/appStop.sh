#!/bin/bash

# 应用名称数组
APPLICATIONS=( 
  'His-System-Web-1.0.jar' 
  'His-Doctor-Service-1.0.jar' 
  'His-Statistic-Service-1.0.jar' 
  'His-ERP-Service-1.0.jar'
)

# 遍历执行
for ((i=0; i<${#APPLICATIONS[@]}; i++ )) 
do
  # 获取pid进程号
  pid=`ps -ef | grep ${APPLICATIONS[i]} | grep 'java -jar' | awk '{printf $2}'`

  # 判断进程号是否存在
  if [ -z $pid ];
    then
      # 不存在则输出提示信息
      echo "应用：${APPLICATIONS[i]} 没有运行!"
    else
      # 存在则杀死进程
      echo "应用：${APPLICATIONS[i]} 进程:$pid 正在停止..."
      `sudo kill -9 $pid`
  fi
done;
