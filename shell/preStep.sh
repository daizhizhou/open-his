#!/bin/bash

# 1、停止进程执行
echo '正在停止jar服务...'
sudo ./appStop.sh

# 2、清除代码和日志
sudo sleep 3s
sudo rm -rf ./code/Open-His
echo 'jar包已被清除...'
sudo rm -rf ./*.log
echo '日志已被清除...'